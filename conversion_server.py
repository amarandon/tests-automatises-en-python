from flask import Flask, request, jsonify


app = Flask(__name__)


@app.route("/")
def celcius_to_kelvin():
    try:
        celcius = float(request.args["celcius"])
    except Exception as exc:
        return {"error": "Invalid celcius value"}
    return jsonify({
        "kelvin": celcius + 273.15
    })


if __name__ == "__main__":
    app.run()
