import requests


def celcius_to_kelvin(celcius):
    response = requests.get("http://localhost:5000", {"celcius": celcius})
    response_data = response.json()
    if "error" in response_data:
        raise Exception(response_data["error"])
    return response_data["kelvin"]
