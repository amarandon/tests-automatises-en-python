---
verticalSeparator: <!--v-->
---

# Tests automatisés en Python

---

## Sommaire

- Pourquoi les tests automatisés ?
- Quand écrire des tests
- Le mot clé assert
- Structurer ses tests avec unittest
- Simuler des systèmes externes avec mock
- Tester sa doc avec doctest
- Auto-détection des tests avec pytest
- Mesurer le taux de couverture

---

## Pourquoi les tests automatisés ?

<!--v-->

#### Motif principal

Pour pouvoir **modifier** son code avec **confiance**.

- ajout de fonctionalité
- correction de bug
- mise à jour de dépendances

<!--v-->

### Motifs supplémentaires

- simuler des erreurs difficiles à reproduire manuellement (erreur d'accès réseau, etc.)
- avoir une vue d'ensemble des cas traités par notre code
- mettre en évidence les dépendences implicites de son code

---

## Quand écrire des tests ?

Avant ou après le code ?

---

## Le mot clé assert

Déclencher *volontairement* une erreur si une affirmation est fausse:

```python
assert celcius_to_kelvin(20) == 293.15
```

---

## Structurer ses tests avec unittest


```python
from unittest import TestCase


class TestConversion(TestCase):
    def test_celcius_to_kelvin(self):
        assert celcius_to_kelvin(20) = 293.15
```

<!--v-->

### Exécuter ses tests avec unittest


```bash
python -m unittest test_conversion.py
```

<!--v-->

### Exercice

- exécuter ce test
- observer l'échec
- écrire la fonction
- constater le succès

<!--v-->

### Assertions fournies par unittest


```python
from unittest import TestCase


class TestConversion(TestCase):
    def test_celcius_to_kelvin(self):
        self.assertEqual(celcius_to_kelvin(20), 293.15)

    def test_celcius_to_kelvin_below_absolute_zero(self):
        self.assertRaises(ValueError, celcius_to_kelvin,
                          -273.16)
```

<!--v-->

### Assertions sous forme de *context manager*


```python
def test_celcius_to_kelvin_below_absolute_zero(self):
    with self.assertRaises(ValueError):
        celcius_to_kelvin(-273.16)
```

<!--v-->

### Préparation et nettoyage de l'environement

```python
class TestFileConversion(TestCase):

    def setUp(self):
        # Créer fichier de données en entrée

    def tearDown(self):
        # Supprimer le fichier de résultats
```

<!--v-->

### Exercice

Tester et écrire une fonction qui lit un fichier de températures en celcius et
le convertit en fichier de températures en kelvin.

---

## Simuler des systèmes externes avec mock

- remplacer des dépendences externes par des objets qu'on controle
- vérifier les interactions entre notre code et ces objets

<!--v-->

### Configurer ce que renvoie un mock

```pycon
>>> mock = Mock(return_value=42)
>>> mock()
42
```

<!--v-->

### Vérifier les interactions avec un mock

```pycon
>>> mock.call_count
1
>>> mock.assert_called_with()
>>> mock.assert_called_with('hello')
[...]
AssertionError: expected call not found.
Expected: mock('hello')
Actual: mock()
```

<!--v-->

### Exemple: intégration avec un API web

- reçoit la température en celcius par la *query string*
- renvoie JSON contenant la température en kelvin

<!--v-->

### Exemple de serveur

```python
from flask import Flask, request, jsonify
app = Flask(__name__)

@app.route("/")
def celcius_to_kelvin():
    celcius = float(request.args["celcius"])
    return jsonify({
        "kelvin": celcius + 273.15
    })

app.run()
```

<!--v-->

### Le décorateur patch

```python
@mock.patch('conversion.requests')
def test_external_api_request(self, mock_requests):
    mock_requests.get.return_value.json.return_value = {
        "kelvin": 0
    }
    result = conversion.celcius_to_kelvin(-273.15)
    self.assertEqual(result, 0)
    mock_requests.get.assert_called_with(
        "http://localhost:8000", {"celcius": -273.15}
    )
```

<!--v-->

### Exercice

Gérer les valeurs en celcius invalides.

---

## Doctest

Des docstrings vérifiables

```python
def celcius_to_kelvin(n):
    """
    Example:

    >>> celcius_to_kelvin(20)
    293.15
    """
```

```bash
python -m doctest [-v] test_conversion.py
```

<!--v-->

### Exercice

- Introduire un bug dans le code et  observer l'échec du doctest.
- Consulter le docstring avec un outil de documentation (`help`, `pydoc`, etc.)

---

## Pytest 

La commande `pytest` trouve et exécute nos tests automatiquement:

```
pytest -v
```

Même les doctests ?

<!--v-->

### Tester sans unittest


```python
def test_foo():
    assert 21 * 2 == 43
```

<!--v-->

### Exercice

- Comparer les messages d'erreur affichés par `assert` suivant qu'on utilise `pytest` ou pas.
- Modifier un autre test pour qu'il n'utilise plus unittest

---

## Mesurer le taux de couverture

<!--v-->

### Couverture de code

Techniquement coverage n'est pas limité à la couverture de code.

```bash
$ coverage run dummy_code.py
$ coverage report --show-missing
Name            Stmts   Miss  Cover   Missing
---------------------------------------------
dummy_code.py       5      1    80%   6
---------------------------------------------
TOTAL               5      1    80%
```

```bash
$ coverage html
```

<!--v-->

### Couverture de test

En pratique, on veut souvent savoir quelles parties du code ne sont pas testées.

```bash
$ coverage run --branch -m unittest test_conversion.py 
......
----------------------------------------------------------------------
Ran 6 tests in 0.004s

OK
$ coverage report --show-missing
Name                 Stmts   Miss Branch BrPart  Cover   Missing
----------------------------------------------------------------
conversion.py            7      0      2      0   100%
test_conversion.py      60      1     24      1    98%   50->52, 84
----------------------------------------------------------------
TOTAL                   67      1     26      1    98%
```
<!--v-->

### Utiliser coverage avec Pytest


```bash
pytest --cov-branch --cov
[...]
---------- coverage: platform linux, python 3.10.2-final-0 -----------
Name                 Stmts   Miss Branch BrPart  Cover
------------------------------------------------------
conversion.py            7      0      2      0   100%
test_conversion.py      60      0     24      1    99%
------------------------------------------------------
TOTAL                   67      0     26      1    99%
```
