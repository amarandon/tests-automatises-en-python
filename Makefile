watch:
	reveal-md tests-automatises-python.md -w --port 9876

static:
	reveal-md tests-automatises-python.md --static
	cp -r _static tests-automatises-python
	zip -r tests-automatises-python.zip tests-automatises-python
