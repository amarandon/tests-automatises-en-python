import os
from unittest import TestCase
from unittest import mock
import conversion


def celcius_to_kelvin(n):
    """
    Example:

    >>> celcius_to_kelvin(20)
    293.15
    """
    if n < -273.15:
        raise ValueError("Celcius temperature must be greater or equal to −273,15")
    return n + 273.15


def convert_file(source_path, destination_path):
    with open(source_path) as source, open(destination_path, "w") as dest:
        for line in source:
            celcius = float(line.strip())
            kelvin = celcius_to_kelvin(celcius)
            print(kelvin, file=dest)


class ConversionTest(TestCase):
    def test_celcius_to_kelvin(self):
        self.assertEqual(celcius_to_kelvin(20), 293.15)

    def test_celcius_to_kelvin_below_absolute_zero(self):
        with self.assertRaises(ValueError):
            celcius_to_kelvin(-273.16)


class TestFileConversion(TestCase):
    parent_dir = os.path.dirname(os.path.abspath(__file__))
    test_data_dir = os.path.join(parent_dir, "data")
    source_filepath = os.path.join(test_data_dir, "source_file.txt")
    destination_filepath = os.path.join(test_data_dir, "destination_file.txt")

    def setUp(self):
        with open(self.source_filepath, "w") as fp:
            print(20, file=fp)
            print(0, file=fp)
            print(-273.15, file=fp)
            print(1000, file=fp)

    def tearDown(self):
        if os.path.exists(self.source_filepath):
            os.remove(self.source_filepath)
        if os.path.exists(self.destination_filepath):
            os.remove(self.destination_filepath)

    def test_same_value_count(self):
        self.assertFalse(os.path.exists(self.destination_filepath))
        convert_file(self.source_filepath, self.destination_filepath)
        with open(self.destination_filepath) as fp:
            data = fp.readlines()
        self.assertEqual(len(data), 4)

    def test_absolute_zero(self):
        convert_file(self.source_filepath, self.destination_filepath)
        with open(self.destination_filepath) as fp:
            data = fp.readlines()
        self.assertEqual(float(data[2].strip()), 0)

    @mock.patch('conversion.requests')
    def test_external_api_request_success(self, mock_requests):
        mock_requests.get.return_value.json.return_value = {"kelvin": 0}
        self.assertEqual(conversion.celcius_to_kelvin(-273.15), 0)
        mock_requests.get.assert_called_with("http://localhost:5000", {"celcius": -273.15})

    @mock.patch('conversion.requests')
    def test_external_api_request_failure(self, mock_requests):
        mock_requests.get.return_value.json.return_value = {"error": "Invalid celcius value"}
        with self.assertRaises(Exception) as context_manager:
            conversion.celcius_to_kelvin(-273.15)
        self.assertEqual(str(context_manager.exception), "Invalid celcius value")



def test_foo():
    assert 21 * 2 == 42
