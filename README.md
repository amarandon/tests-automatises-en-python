Install [reveal-md](https://github.com/webpro/reveal-md).

Start server with:

    make watch

Build static archive:

    make static
